# Gulp Starter [![Dependency Status](https://david-dm.org/racse1/gulp-starter.svg)](https://david-dm.org/racse1/gulp-starter)

Gulp Starter is a starter template with Gulp.

## Getting started

### System requirements

* Node.js >= 4.2.4
* npm >= 2.14.12

## License

Code available under [MIT license](LICENSE).
